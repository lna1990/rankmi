class Comment < ApplicationRecord
  belongs_to :user
  belongs_to :requirement

  validates :body, presence: true # Valido que tenga comentario al crearlo.
end
