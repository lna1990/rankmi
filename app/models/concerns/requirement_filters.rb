require 'active_support/concern'

module RequirementFilters
  extend ActiveSupport::Concern
  included do

  end

  module ClassMethods
    def filter(filtering_params)
      results = self.where(nil)
      filtering_params.each do |key, value|
        results = results.public_send(key, value) if value.present?
      end
      results
    end

    def filter_interface(params)
      results = Requirement.where(nil)
      params = ActionController::Parameters.new.merge(params)
      filter_by_params(params, results)
    end

    private

    def filter_by_params(params, results)
      params.each do |filter_name, value| # Itero la lista de parametros que haya
        if respond_to?(filter_name, true)
          results = send(filter_name, results, value) unless value.empty?
        else
          raise ArgumentError, "Unknown filter #{filter_name}."
        end
    end
    results
    end

    def by_vigency(results,value)
      return results.order('title ASC') if value == 'Alfabetico'
      return results.sort_by{|requirement| -requirement.score} if value == 'Puntaje'
    end
  end
end
