class Requirement < ApplicationRecord
  include RequirementFilters
  has_many :votes, dependent: :destroy
  has_many :comments, dependent: :destroy

  validates :title, :description, presence: true # Titulo y descripcion obligatorios

  # Calculo el total de votos de un requerimiento, teniendo en cuenta la entidad votes
  def score
     votes.where(vote_type: 1).count - votes.where(vote_type: 0).count
  end
end
