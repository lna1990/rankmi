class Vote < ApplicationRecord
  belongs_to :user
  belongs_to :requirement

  validates :requirement, uniqueness: { scope: :user } # Validaciones para votar una vez sola por user un requerimiento
  validates :user, uniqueness: { scope: :requirement } # Validaciones para votar una vez sola por user un requerimiento
end
