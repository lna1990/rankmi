module ApplicationHelper
  # to create internal links
  def side_link(link_text, link_path, icon_class = '', klass = '', html_options = {}, extra = '')
    link_class = html_options[:class]
    html_options[:class] = link_class
    html_options[:title] = link_text
    klass += current_page?(link_path) ? 'active' : url_for(link_path)
    content_tag(:li, class: klass) do
      (link_to link_path, html_options do
        "<i class='fa fa-fw fa-#{icon_class}'></i> <span> #{link_text} </span>".html_safe
      end) +
        extra.html_safe
    end
  end

end
