initialize_flash_messages = function(e, flashMessages) {

  $('#snackbar-container > div').remove();

  $('#snackbar-container > span, #snackbar-container > div').remove();

  $.each(flashMessages, function(key, value) {
    var style;
    style = '';
    switch (key) {
      case 'success':
        style = 'callout callout-success bg-primary';
        break;
      case 'danger':
        style = 'callout callout-danger bg-red';
        break;
      case 'error':
        style = 'callout callout-danger bg-primary';
        break;
      default:
        style = 'callout';
        break;
    }
    $.snackbar({
      content: value,
      style: style,
      timeout: 10000
    });
  });
};

// ------------------------------------------------------ //
initialize_snackbar = function() {
  if ($('#snackbar-container').length == 0) {
    $("body").append("<div id=snackbar-container/>");
  }
  if ($('.snackbar-message').length > 0) {
    $('.snackbar-message').snackbar('show');
  }
};


initialize_select2_simple = function(object) {
  $(".select2_simple").select2({
    placeholder: 'Buscar',
    allowClear: true,
    width: "97%",
    theme: "bootstrap",
    escapeMarkup: function(markup) {
      return markup;
    },
    cache: true, // let our custom formatter work
  });
};

$(document).ready(function() {
  initialize_flash_messages();
  initialize_snackbar();
});
