class CommentsController < ApplicationController

  def index; end

  def edit; end

  private

  def comment_params
    params.require(:comment).permit(:body, :user_id, :requirement_id, :utf8, :authenticity_token, :commit)
  end
end
