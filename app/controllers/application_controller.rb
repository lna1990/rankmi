class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_current_user

def set_current_user
  if @current_user
    @current_user
  else
    # El enunciado aclara que estan cargados los usuarios, simulo directamente
    @current_user = User.new(
      id: session[:session_id],
      name: 'Lucas',
      surname: 'Albo',
      email: 'luucasunlp@gmail.com'
    )
  end
end
end
