class VotesController < ApplicationController

  def create
    vote = Vote.new(vote_params)
    vote.requirement = Requirement.find(params[:requirement_id])

    if vote.save
      redirect_to requirements_path
      flash[:success] = 'Voto Agregado!'
    else
      redirect_to requirements_path
      flash[:danger] = 'Voto Rechazado!'
    end
  end

  private

  def vote_params

     params.require(:vote).permit(:user_id, :requirement_id, :vote_type)
  end
end
