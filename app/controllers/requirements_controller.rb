class RequirementsController < ApplicationController
    before_action :find_requirement, only: %i[ edit update destroy]

    def index
      @requirements = Requirement.all # Usaria Presenter para poder enviar directamente a la vista
                                      # el modelo con todos sus elementos, podria agregar will_paginate, decorator
    end

    def new
      @requirement = Requirement.new
      respond_to do |format|
        format.html
        format.js
      end
    end

    def show; end

    def create
      @requirement = Requirement.new(requirement_params)
      if @requirement.save
        redirect_to requirements_path
        flash[:success] = 'Requerimiento Creado Correctamente'
      else
        flash[:danger] = 'Campo/s Obligatorios Incompleto/s'
        render :new
      end
    end

    def edit; end

    def destroy
      if @requirement.destroy
         flash[:success] = 'Requerimiento Eliminado'
         redirect_to requirements_path
      else
        flash[:danger] = 'Requerimiento no pudo ser eliminado'
        redirect_to requirements_path
      end
    end

    def update
      respond_to do |format|
        if @requirement.update(requirement_params)
          format.html do
            redirect_to requirements_path, notice: 'Requerimiento editado con exito'
          end
          format.json do
            render :show, status:
                               :ok, location: @requirement
          end
        else
          flash[:danger] = 'Campo/s Obligatorios Incompleto/s'
          render :edit

        end
      end
    end

    def vote
      @requirement = Requirement.find(params[:id])
      vote_saved = @requirement.votes.create(requirement_id: @requirement.id, user_id: params[:id], vote_type: params[:vote_type])
      redirect_to requirements_path
      flash[:success] = 'Votó exitoso' if vote_saved.errors.empty?
      flash[:danger] = 'Usted ya votó en este requerimiento'
    end

    def down_vote
      # Esto podria mejorarlo y directamente utilizar el action vote,
      # ya que puedo diferenciar con el tipo de voto (voto_type). Por cuestiones de tiempo no lo refactoricé
      @requirement = Requirement.find(params[:id])
      vote_saved  = @requirement.votes.create(requirement_id: @requirement.id, user_id: params[:id], vote_type: params[:vote_type])
      redirect_to requirements_path
      flash[:success] = 'Votó exitoso' if vote_saved.errors.empty?
      flash[:danger] = 'Usted ya votó en este requerimiento'
    end

    def filter
      @requirements = Requirement.filter_interface(params[:requirement])
      render 'requirements/filter.js.erb'
    end

    def new_comment
      @comment = Comment.new
    end

    def create_comment
      @comment = Comment.new(body: params[:comment][:body], user_id: @current_user.id, requirement_id: params[:id] )
      if @comment.save
        redirect_to requirements_path
        aux = flash[:success] = 'Comentario Creado Correctamente'
      else
        aux = flash[:danger] = 'Campo/s Obligatorios Incompleto/s'
        render :new
      end
    end

    private

    def filtering_params(params)
      params.permit(:by_vigency, :title, :votes)
    end

    def find_requirement
      @requirement = Requirement.find(params[:id])
    end

    def requirement_params
      params.require(:requirement).permit(:title, :description,:user_id,
                     :requirement_id, :vote_type, :authenticity_token, :id)
    end

    def vote_params
      params.permit(:user_id, :requirement_id, :vote_type, :id)
    end

    def comment_params
      params.permit(:user_id, :body, :user_id, :requirement_id, :utf8, :authenticity_token, :commit)
    end
end
