Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # root 'requirements#index'
  root to: redirect('/login')

  # Routes for Google authentication
  get 'login', to: redirect('/auth/google_oauth2'), as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  get 'auth/:provider/callback', to: 'sessions#googleAuth'
  get 'auth/failure', to: redirect('/')
  get 'requirements', to: 'requirements#index'


  resources :comments
  post 'comments/new'
  resources :requirements do
    member do
      post 'vote'
      get 'vote'
      post 'down_vote'
      get 'down_vote'
      post 'new_comment'
      get 'new_comment'
      post 'create_comment'
      get 'create_comment'
    end
 end

 get 'requirements/filter'
 post 'requirements/filter'
resources :votes, only: [:create, :destroy]
end
